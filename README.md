# README #

This repository contains Circos_alignment.py and etc_template/ 
Together, these can take the results.txt created by VirulenceFinder, ResFinder or PlasmidFinder 
and create a circular visualization of the alignement results. This visualization 
shows one circular output for the alignment of each hit (eg. resistance gene) found by the Finder service.

The tool is still under developement so any comments or suggestions are much appreciated. 

## Content of Repository ##
1. Circos_alignment.py (the main script)
2. etc_template/ (a directory with files used by Circos_alignement.py)

## Installation ##
```bash
# Go to wanted location for Circos Alignment 
cd /path/to/some/dir

# Clone and enter the Circos Alignment directory
git clone https://git@bitbucket.org/genomicepidemiology/Circos_alignment.git
cd Circos_alignment
```

### Dependencies ###
The program runs a separate program called Circos. Download the newest version here: http://circos.ca/software/download/circos/

Circos citation: Krzywinski, M. et al. Circos: an Information Aesthetic for Comparative Genomics. Genome Res (2009) 19:1639-1645. 


## Usage ##
The program takes a results.txt file from either VirulenceFinder, ResFinder or PlasmidFinder 
(https://cge.cbs.dtu.dk/services/). 

### Command line options ###
```bash
# Required arguments
-r, --result_file		results.txt created by the finder service. 
-f, --file_dir			Relative path to etc_template/ 
-c, --circos_path		Path to executable Circos program.


# Optional arguments
-tmp, --tmp_dir		Relative path to directory for storage of the input files for Circos created by the program. (Default is .)
-o, --output_dir		Relative path to wanted location of Circos output. (Default is .)
```

An example: 
```bash
# Run program
python3 Circos_alignment.py -r /path/to/results.txt -f etc_template/ -c circos-0.69-9/bin/circos
```

The program will create two new directories in the specified tmp location for each hit in the results.txt file; etc/ and data/
These store input files for Circos created by the program based on the files in etc_template/
They can be deleted after each run (and should be, if you want to run the program again). But always keep etc_template/

The program will also create two image files in the specified output location containing the circular output from Circos. 


## Contact ##
Sofie Theisen Honore, s174352@student.dtu.dk

CGE Helpdesk, food-cgehelp@dtu.dk
